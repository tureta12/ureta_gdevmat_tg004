void setup()
{
  size(1920, 1080, P3D);
  // x = width / 2; This was the original code but we could just move the camera so that the origin is at the center instead of top-left.
  // y = height / 2; Problem of not moving the camera is that the y axis is positive going downwards. So we flipped it. 
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, - 1, 0);
}

int x;
int y;

// Notice the x and y outside. Java isn't a top down language unlike c++
// We don't put int x inside setup(). Why?
// Because we want it to be global. Putting this inside setup() means it will only be called inside setup()
// We don't want int x inside draw() because it means it'll be called every frame. 

void draw()
{
  // Called every frame
  // circle(x, y, 50);
  // y++;
  background(255); // This clears the screen by overlaying everything with white per call. Like a clear screen. 
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  drawMovingSineWave();
}

void drawCartesianPlane()
{
  line(300, 0, -300, 0);
  line(0, 300, 0, -300);

  for (int i = -300; i <= 300; i+=10) // For the points
  {
    line(i, -5, i, 5);
    line (-5, i, 5, i);
  }
}

void drawLinearFunction()
{
  /*
   f(x) = x + 2
   Let x be 4, then y = 6; (4, 6)
   */
  for (int x = -200; x <= 200; x++)
  {
    circle(x, x + 2, 2);
  }
}

void drawQuadraticFunction()
{
  /* 
   f(x) = x^2 + 2x - 5
   */
  for (float x =-300; x <= 300; x+=0.1f)
  {
    circle(10 * x, ((x * x) + (2 * x) - 5), 2);
  }
}

float radius = 69;
void drawCircle()
{
  for (int i = 0; i < 360; i++)
  {
    circle((float)Math.cos(i) * radius, (float)Math.sin(i) * radius, 1);
  }
}

float amplitude = 30;
float time = 0;
float deltaTime = 0.1;
float frequency = 0.05;
void drawMovingSineWave()
{
  // Amplitude * sin (frequency * time)
  // f(t) = sin(t) 
  for(float x = -800; x <= 800; x+=0.1f)
  {
    circle(x, (float)Math.sin( (x + time) * frequency) * amplitude, 1);
    time += deltaTime;
  }
}
