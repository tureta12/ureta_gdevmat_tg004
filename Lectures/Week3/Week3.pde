void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 
    0, 0, 0, 
    0, - 1, 0);
  background(0);
}

/* Gaussian = Normal Distribution, clusters around the mean
Standard Deviation
     Range -> HighestValue - LowestValue
     Variance -> Average of total squared difference of each value and the mean
          SetA {-10, 0, 10, 20, 30), mean = 10
               Variance: = (-10 - 10)^2 + (0 - 10)^2 + (10 - 10)^2 + (20 - 10)^2 + (30 - 10)^2
               Then get the square root of the result
*/

void draw()
{
  float gauss = randomGaussian();
  float standardDeviation = 120;
  float mean = 0;
  
  noStroke();
  fill(255, 10);
  circle(0, 0, 30);
}
