void setup()
{
  size(1920, 1080, P3D);
  background(255);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.80f),
        0, 0, 0,
        0, -1, 0);
}

float dt = 0;

void draw()
{
  float n = noise(dt); //noise geneerates from 0.0 - 1.0, BUT processing has map()
  float x = map(n, 0, 1, 0, Window.top);
  rect(Window.left + (dt * 100), Window.bottom, 1, x);
  
  dt += 0.01f;
}
