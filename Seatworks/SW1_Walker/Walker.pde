class Walker
{
  // Java's classes are public by default
    float xPosition;
    float yPosition;
    int stepDistance = 15;
    int[][/* x & y*/] walkOptions = { 
                            /*MoveUp*/{0,stepDistance}, 
                            /*MoveDown*/{0,-(stepDistance)},
                            /*MoveLeft*/{-(stepDistance),0}, 
                            /*MoveRight*/{stepDistance,0}, 
                            /*MoveUpperLeft*/{-(stepDistance),stepDistance},
                            /*MoveLowerLeft*/{-(stepDistance),-(stepDistance)},
                            /*MoveUpperRight*/{stepDistance,stepDistance},
                            /*MoveLowerRight*/{stepDistance,-(stepDistance)}
                             };

    void render()
    {
      circle(xPosition, yPosition, 50);
    }
    
    void randomWalk()
    {
      int decision = floor(random(8));
      xPosition += walkOptions[decision][0];
      yPosition += walkOptions[decision][1];
      
      // Random colors
     color circleColor = color( floor(random(256)), floor(random(256)), floor(random(256)) ); 
     fill(circleColor);
    }
}
