void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, - 1, 0);
  background(255);
}

Walker walker = new Walker();

void draw()
{
  noStroke(); // No stroke
  walker.render();
  walker.randomWalk();
 
  /*
  // Syntax for random by default is random(high) wherein high is the upper limit
  
  float randomNumber = random(4); // random is always a float, so how do we make it into an integer?
  int randomNumber = floor(random(4)); // Floor rounds down to the nearest whole number 
  println(randomNumber);
  */
}
