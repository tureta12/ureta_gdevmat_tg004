void setup()
{
  size(1920, 1080, P3D);
  background(0);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.80f),
        0, 0, 0,
        0, -1, 0);
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x,y);
}

float handleScale = 7;
float saberLength;
float frame;
float r = 173;
float g = 216;
float b = 230;
float white = 0;
float outlineOffset = 8;
float strokeSize = 1;

float r_t = 2;
float g_t = 4;
float b_t = 3;
float w_t = 3;
float stroke_t = 1;

void draw()
{
  // FEATURE TO ADD. OUTLINE STROKE PERLIN NOISE TOO
  
  background(0);
  
  r = map(noise(r_t), 0, 1, 130, 173);
  g = map(noise(g_t), 0, 1, 170, 216);
  b = map(noise(b_t), 0, 1, 190, 230);
  white = map(noise(w_t), 0, 1, 200, 255);
  strokeSize = map(noise(stroke_t), 0, 1, 10, 16);
      
  Vector2 mouse = mousePos();
  mouse.normalize();
  mouse.mult(saberLength);
  
  // Opening animation
  frame++;
  if(frame < 35)
  {
    saberLength += 15 ;
  }
  
  // Lightsaber
    // First pass (for Outline)
  strokeWeight(strokeSize);
  stroke(r, g, b);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
    // Second Pass
  strokeWeight(7);
  stroke(white, white, white);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);

  // Handle
  strokeWeight(18);
  stroke(30, 30, 30); // Dark Grayish
  line(-mouse.x / handleScale, -mouse.y / handleScale, mouse.x / handleScale, mouse.y / handleScale);
  
  r_t += 0.1;
  g_t += 0.1;
  b_t += 0.1;
  w_t += 0.1;
  stroke_t += 0.1;
  
  println(mouse.mag());
  
  // Sounds
  //minim = new Minim(this);
}
