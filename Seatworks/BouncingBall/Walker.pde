class Walker
{
  float diameter = 100;
  int xDirection = 1;
  int yDirection = 1;
  color circleColor;
  
  public Vector2 position = new Vector2();
  public Vector2 velocity = new Vector2(10, 12);
    
    /*
    Walker()
    {
      position = new Vector2();
    }
    
    Walker(float x, float y)
    {
      position = new Vector2(x, y);
    }
    
    Walker(Vector2 position)
    {
      this.position = position;
    }
    */
    
   public void render()
    {
      circle(position.x, position.y, diameter);
    }
    
    void randomWalk()
    {
      position.add(velocity);
      
      if (position.x > Window.right || position.x < Window.left)
      {
        xDirection *= -1;
        circleColor = color(random(0, 256), random(0, 256), random(0, 256));
        fill(circleColor);
      }
      
      if ( position.y > Window.top || position.y < Window.bottom)
      {
        yDirection *= -1; 
         circleColor = color(random(0, 256), random(0, 256), random(0, 256));
         fill(circleColor);
      }
    }
    
    
    
  
  
  
}
