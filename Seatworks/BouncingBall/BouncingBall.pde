void setup()
{
  size(1920, 1080, P3D);
  background(0);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.80f),
        0, 0, 0,
        0, -1, 0);
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x,y);
}

// Walker walker = new Walker(); // bouncing ball

void draw()
{
  /* Bouncing ball code
  background(0);
  noStroke();
  walker.randomWalk();
  walker.render();
  */
  
  // Lightsaber
  background(255);
  
  Vector2 mouse = mousePos();
  mouse.normalize();
  mouse.mult(800);
  
  strokeWeight(15);
  stroke(200, 0, 0);
  line(0, 0, mouse.x, mouse.y);
  
  println(mouse.mag());
}
