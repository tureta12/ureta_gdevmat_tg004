void setup()
{
  size(1920, 1080, P3D);
  background(0);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.80f),
        0, 0, 0,
        0, -1, 0);
}

Walker walker = new Walker();

void draw()
{ 
  noStroke(); // No stroke
  walker.randomWalk();
  walker.render();
}
