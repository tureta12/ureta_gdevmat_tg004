class Walker
{
    float r_t = 0.9;
    float g_t = 1.1;
    float b_t = 1.4;
    float dDiameter = 1.5;
    float dXDistance = 0.6;
    float dYDistance = 1.3;
    
    float xPosition = 0;
    float yPosition = 0;
    float diameter = 0;
    float r = 0;
    float g = 0;
    float b = 0;
    
    void render()
    {
      circle(xPosition, yPosition, diameter);
    }

    void randomWalk()
    {
      yPosition = map(noise(dYDistance), 0, 1, Window.bottom, Window.top);
      xPosition = map(noise(dXDistance), 0, 1, Window.left, Window.right);
      diameter = map(noise(dDiameter), 0, 1, 30, 75);
      
      dXDistance += 0.01;
      dYDistance += 0.01;
      dDiameter += 0.05; 
      
      // Random colors
      r = map(noise(r_t), 0, 1, 0, 255);
      g = map(noise(g_t), 0, 1, 0, 255);
      b = map(noise(b_t), 0, 1, 0, 255);
      color circleColor = color( r, g, b); 
      fill(circleColor);
      r_t += 0.1;
      g_t += 0.2;
      b_t += 0.3;
    }
}
